#! /bin/bash

# install script by bentomas at http://git.benjaminthomas.org/dotfiles (thanks benj!)
# modified to be a bit simpler
#
# this install script isn't a config file, obviously.  You can download it and
# pipe it into bash to automatically install all the files.  Makes getting set
# up on a new computer a breeze!
#
# if you want to test, change the DEST var at line 15

# check arguments, if not local, download files
current=`pwd`
#cd $1
CONFIG_FOLDER=`pwd`

# install into the home directory, change this for testing, mostly
DEST=~
TRASH=~/.Trash

# get the files that we're eventually going to overwrite
files=( `find $CONFIG_FOLDER/* | grep -v "\[.*\]$" | tr '\n' ' '`)
index=0

# remove old files we added, so they don't get orphaned
if [ -f $DEST/.config_files_installed ]; then
  while read line ; do
    line=${line:${#DEST}+2}
    toRemove[$index]="$line"
    index=$(($index+1))
  done < $DEST/.config_files_installed
fi

# add files we're going to overwrite, so the overwritten files get properly
# backed up
for filename in ${files[@]}; do
  toRemove[$index]=${filename:${#CONFIG_FOLDER}+1}
  index=$(($index+1))
done

# this serves two purposes, cleaning up our home directory for when we change
# dotfiles, and also backing up every file we overwrite
echo "removing old files..."
for filename in ${toRemove[@]}; do
  dirname=( `dirname "$filename"` )

  # if the file already exists then we need to remove it
  if [ -f $DEST/.$filename ]; then
    # store old files in a common dir, so it would be easier to go back and
    # retrieve them (and even reinstall them)
    mkdir -p $CONFIG_FOLDER/old_files

    if [ -d $DEST/.$dirname -a "$dirname" != "." -a ! -d  "$CONFIG_FOLDER/old_files/$dirname" ]; then
      echo "mkdir -p \"$CONFIG_FOLDER/old_files/$dirname\""
      mkdir -p "$CONFIG_FOLDER/old_files/$dirname"
    fi

    echo "mv \"$DEST/.$filename\" \"$CONFIG_FOLDER/old_files/$filename\""
    mv "$DEST/.$filename" $CONFIG_FOLDER/old_files/$filename

    if [ -d "$DEST/.$dirname" -a "$dirname" != "." ]; then
      # check to see if the folder is empty
      if [ "$(ls -A $DEST/.$dirname)" = "" ]; then
        echo "rmdir \"$DEST/.$dirname\""
        rmdir "$DEST/.$dirname"
      fi
    fi
  fi
done

# trash the old files
if [ -e $CONFIG_FOLDER/old_files ]; then
  datetime=`date +%d-%H-%M-%S`
  unique="$TRASH/old_files_$datetime"
  echo "mv $CONFIG_FOLDER/old_files $unique"
  mv $CONFIG_FOLDER/old_files $unique
  echo "moved to trash"
fi

rm $DEST/.config_files_installed
echo ""

# make sure .config_files_installed exists
touch $DEST/.config_files_installed
echo "copying current files..."
for filename in ${files[@]}; do
  filename=${filename:${#CONFIG_FOLDER}+1}
  dirname=( `dirname "$filename"` )
  if [ $filename = install.sh ]; then
    continue
  fi


  # make sure the file is a file and not a directory
  if [ -f $CONFIG_FOLDER/$filename ]; then
    if [ $dirname != '.' -a ! -d "$DEST/.$dirname" ]; then
      echo "mkdir -p \"$DEST/.$dirname\""
      mkdir -p "$DEST/.$dirname"
    fi
    echo "$DEST/.$filename" >> $DEST/.config_files_installed
    echo "cp \"$CONFIG_FOLDER/$filename\" \"$DEST/.$filename\""
    cp "$CONFIG_FOLDER/$filename" "$DEST/.$filename"
  fi
done
