# ~/.bashrc: executed by bash(1) for non-login shells.
function parse_git_dirty {
  #[[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo -e '\E[1;31m*'
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit, working directory clean" ]] && echo -e '*'
}
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\1$(parse_git_dirty)/"
}
export PS1='\w[\[\033[0;33m\]$(parse_git_branch)\[\033[0m\]]$ '
#export LD_LIBRARY_PATH="$HOME/local/lib"
export PATH="$HOME/local/bin:$PATH"
export PATH="/usr/local/bin:/usr/local/etc:/usr/local/sbin:~/bin:$PATH"
export EDITOR='vim'

SSH_BIN=$(which ssh)
function wssh(){
    $SSH_BIN -A peter.compernolle@$1.wellspringworldwide.com $2
}

function dssh(){
    $SSH_BIN -A peter.compernolle@$1.wellspringsoftware.net $2
}

# usage:
#   fetch gates
# what it does:
#   it grabs the gates_kms database and adds it to ~/dbs/
function fetch(){
    host=`echo $1 | sed -E "s/_/./g"`
    file=$1.`date +%Y%m%d`.sql.gz
    #dump=`dssh $host "cat /etc/wellspring/sophia/$1-Config.php" | grep -i "db_user',\|db_pass',\|db_name'," | sed -E "N;N;s/^\s*define\('DB_[A-Za-z]{4}',\s*'([A-Za-z0-9_]*)'\);\s*define\('DB_[A-Za-z]{4}',\s*'([A-Za-z0-9_]*)'\);\s*define\('DB_[A-Za-z]{4}',\s*'([A-Za-z0-9_]*)'\);\s*$/mysqldump -h$host.wellspringsoftware.net -u\1 -p\2 \3|gzip > $file/"`
    user=`dssh $host "cat /etc/wellspring/sophia/$1-Config.php" | grep -i "db_user'," | sed -E "s/\s*define\('DB_USER',[\n\t ]*'([A-Za-z0-9_\.]*)'\);/-u\1/"`
    echo "user $user"
    pass=`dssh $host "cat /etc/wellspring/sophia/$1-Config.php" | grep -i "db_pass'," | sed -E "s/\s*define\('DB_PASS',[\n\t ]*'([A-Za-z0-9_\.]*)'\);/-p\1/"`
    echo "password $pass"
    name=`dssh $host "cat /etc/wellspring/sophia/$1-Config.php" | grep -i "db_name'," | sed -E "s/\s*define\('DB_NAME',[\n\t ]*'([A-Za-z0-9_\.]*)'\);/\1/"`
    echo "db name $name"
    dump="mysqldump -h$host.wellspringsoftware.net $user $pass $name|gzip > $file"
    echo $dump
    if [ "$dump" ]; then
        wssh 'wstools-cgx1' "$dump"
        scp peter.compernolle@wstools-cgx1.wellspringworldwide.com:$file ~/dbs/
        wssh 'wstools-cgx1' "rm *.sql.gz"
    fi  
    echo "success!"
}

function ttmux() {
    if tmux has-session -t "$1"; then 
        tmux attach-session -t $1
    else
        tmux new-session -s "$1"
    fi
}

#tmux source ~/.tmux.conf
if which tmux >/dev/null; then
    if ! { [ "$TERM" = "screen" ] && [ -n "$TMUX" ]; } then
        echo '================ welcome ==============='
        if tmux has-session -t "ws"; then
            echo 'current tmux sessions:'
            tmux ls
        else
            tmux new-session -d -s "ws"
            echo 'created new tmux session "ws"'
            tmux ls
        fi
    fi
fi

alias cgrep='find . -type f -name "*.php" -print0 | xargs -0 grep'

#git autocomplete
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi
